import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { IFighter } from './fighterView';
import { IFighterDetail } from './fighterView';
import { getFighterDetails } from './services/fightersService';


export function createFighters(fighters: IFighter[]) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map();

async function showFighterDetails(event: Event, fighter: IFighter) {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: string) {
  return getFighterDetails(fighterId);
}

function createFightersSelector() {
  const selectedFighters = new Map<string, IFighterDetail>();

  return async function selectFighterForBattle(event: Event, fighter: IFighter) {
    const fullInfo : IFighterDetail = await getFighterInfo(fighter._id);
 
    if (<HTMLInputElement>event.target) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const iterator = selectedFighters.values();
      const winner = fight(iterator.next().value, iterator.next().value);
      showWinnerModal(winner); 
    }
  }
}
