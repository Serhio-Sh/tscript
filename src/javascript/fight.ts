import { IFighterDetail } from './fighterView';

export function fight(firstFighter: IFighterDetail, secondFighter: IFighterDetail) {
  let enemy = secondFighter;
  let attacker = firstFighter;

  while(enemy.health > 0)
  {
    enemy.health -= getDamage(attacker, enemy);
    let e = enemy;
    enemy = attacker;
    attacker = e;
  }
  console.log('fight!');
  return attacker;
}

export function getDamage(attacker: IFighterDetail, enemy: IFighterDetail): number {
  let damage: number = getHitPower(attacker) - getBlockPower(enemy);
  return damage;
}

export function getHitPower(fighter: IFighterDetail): number {
  let power: number = fighter.attack * getRandomIntInclusive(1, 2);
  return power;
}

export function getBlockPower(fighter: IFighterDetail): number {
  let power: number = fighter.defense * getRandomIntInclusive(1, 2);
  return power;
}

function getRandomIntInclusive(min: number, max: number): number {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}