export function createElement( elem: IElement) {
  const{tagName, className, attributes} = elem;
  const _element = document.createElement(tagName);
  
  if (className) {
    _element.classList.add(className);
  }

  if(attributes != undefined) {
    Object.keys(attributes).forEach(key => _element.setAttribute(key, attributes[key]));
  }
  return _element;
}

export interface IElement{
  tagName: string, 
  className?: string,
  attributes?: any
}