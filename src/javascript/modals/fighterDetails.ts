import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import {IFighterDetail} from '../fighterView';


export  function showFighterDetailsModal(fighter: IFighterDetail) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal( title, bodyElement );
}

function createFighterDetails(fighter: IFighterDetail) {
  const { name } = fighter;
  const { health } = fighter;
  const { attack } = fighter;
  const { defense } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const healthElement = createElement({ tagName: 'span',className: 'fighter-health' });
  const attackElement = createElement({ tagName: 'span',className: 'fighter-attack' });
  const defenseElement = createElement({ tagName: 'span', className:'fighter-defense' });
  
  // show fighter name, attack, defense, health, image

  nameElement.innerText = 'Name: '+name+'\n';
  healthElement.innerText = 'Health: '+health+'\n';
  attackElement.innerText = 'Attack: '+attack+'\n';
  defenseElement.innerText = 'Defense: '+defense+'\n';
  fighterDetails.append(nameElement, healthElement, attackElement, defenseElement );

  return fighterDetails;
}

