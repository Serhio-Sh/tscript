import { callApi } from '../helpers/apiHelper';
import {IFighterDetail} from '../fighterView';

export async function getFighters(){
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: string) {
  // endpoint - `details/fighter/${id}.json`;
  try {
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET');
    
    return <IFighterDetail>apiResult;
  } catch (error) {
    throw error;
  }
}

