export function fight(firstFighter, secondFighter) {
    let enemy = secondFighter;
    let attacker = firstFighter;
    while (enemy.health > 0) {
        enemy.health -= getDamage(attacker, enemy);
        let e = enemy;
        enemy = attacker;
        attacker = e;
    }
    console.log('fight!');
    return attacker;
}
export function getDamage(attacker, enemy) {
    let damage = getHitPower(attacker) - getBlockPower(enemy);
    return damage;
}
export function getHitPower(fighter) {
    let power = fighter.attack * getRandomIntInclusive(1, 2);
    return power;
}
export function getBlockPower(fighter) {
    let power = fighter.defense * getRandomIntInclusive(1, 2);
    return power;
}
function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
