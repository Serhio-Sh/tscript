import { createName } from '../fighterView';
import { createImage } from '../fighterView';
import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
export function showWinnerModal(fighter) {
    const title = 'WINNER!';
    let bodyElement = createWinnerModal(fighter);
    showModal(title, bodyElement);
}
export function createWinnerModal(fighter) {
    const { name, source } = fighter;
    const nameElement = createName(name);
    const imageElement = createImage(source);
    const fighterContainer = createElement({ tagName: 'div', className: 'fighter' });
    console.log('showWinner:' + fighter.name);
    fighterContainer.append(imageElement, nameElement);
    return fighterContainer;
}
